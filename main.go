package main

import (
	"fmt"
	"iphelge/iphelge"
	"github.com/kataras/iris"
)

func main() {
	fmt.Printf("Webserver for ip.helge.net, version %s\nBuilt using iris-go version %s\n", iphelge.Version, iris.Version)

	c := iphelge.LoadConfigurationArgs()
	if(c.Debug) {
		fmt.Println(c)
	}
	iphelge.StartWebServer()
}