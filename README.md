# ip.helge.net

The code for the ip.helge.net website with the necessary files to build the package and run it on the web server.

## How to run on your own infrastructure

The server uses three (or more hostnames):

* ip.helge.net - this is the main / frontend page
* ip4.helge.net - this is the page that only resolves over IPv4
* ip6.helge.net - this is the page that only resolves over IPv6

The web server does not check for hostnames when receiving traffic. But the main page sends back links to both ip4.helge.net and ip6.helge.net. The prefix (ip4/ip6) is hardcoded, but you can change the suffix from helge.net by using the command line paramter -urlbase=mybase.org

## Configuring the web server

The web application is configurable. There are three ways to configure the application:

1. A JSON config file that configures the [IRIS](https://docs.iris-go.com/configuration.html) framework.
2. Lots of command line options to the application itself. Run with -h to see them all and their default values.
3. A JSON config file for the application. This file is located in the application directory and is loaded automatically if present.

All settings are applied in the order shown above. Last setting wins. 

## How to build for Docker

A sample file is included that shows how to dockerize this application. Alpine is used as the base image, mostly to get root certificates for TLS validation. If you don't need that you can use busybox as the base image.

When you compile Go applications for Docker you should make sure the application is statically linked. To build your application this way build the application using somehting like this:

```CGO_ENABLED=0 go build -a -installsuffix cgo -o main```

You can read more about this [here](https://blog.codeship.com/building-minimal-docker-containers-for-go-applications/).

To build with docker run something like

```docker build -t iphelge .```

## Using haproxy

A sample configuration file is included that shows how to run this application behind haproxy.

* In the file iris.yml the X-Forwarded-For header is enabled.
* haproxy.cfg is configured to send this header.

## Using nginx

A configuration file for nginx is provided that shows how to use nginx as a frontend. Some static folders are also offloaded from the application. The header X-Forwarded-For is also added.
