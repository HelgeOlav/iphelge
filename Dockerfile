FROM alpine
LABEL maintainer "Helge Olav Helgesen <docker-maintainer@helge.net>"
EXPOSE 8080
RUN apk update --no-cache ; apk upgrade ; apk add ca-certificates
COPY assets/ /assets/
COPY main /iphelge
CMD ["/iphelge"]