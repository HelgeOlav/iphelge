package iphelge

import (
	"github.com/kataras/iris"
	"strconv"
)

// at this point we configure the web server
func InitWebServer() *iris.Application {
	app := iris.New()

	if cfg.Debug {
		app.Logger().SetLevel("debug")
		app.Use(logger)
	}
	app.Any("/", frontPage)
	app.OnErrorCode(iris.StatusNotFound, notFoundHandler)
	app.RegisterView(iris.HTML(cfg.WebView, ".html"))
	app.StaticWeb("/", cfg.WebFolder)
	app.Get("/IP", getIP)
	app.Head("/IP", getIP)

	return app
}

func StartWebServer() {
	app := InitWebServer()
	portString := ":" + strconv.Itoa(cfg.WebPort)
	app.Run(iris.Addr(portString), LoadIrisConfiguration())
}