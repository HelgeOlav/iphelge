package iphelge

import (
	"flag"
	"github.com/kataras/iris"
)

// This file handles all configurable options

type Configuration struct {
	// the folder that we find the static content we want to serve
	WebFolder string
	// the folder with the views
	WebView string
	// what port to listen on
	WebPort int
	// if we should enable some debugging
	Debug bool
	// IRIS JSON configuration file
	IrisCfg string
	// base URL (adding ip4, ip6 for connectivity checkers
	UrlBase string
}

// the configuration variable
var cfg Configuration

// Return the global configuration object
func GetConfiguration() *Configuration {
	return &cfg
}

// loads configuration arguments from command line
func LoadConfigurationArgs() *Configuration {
	portPtr := flag.Int("port", 8080, "port number to bind to")
	WebPtr := flag.String("webfolder", "assets/web", "Default directory for web contents")
	DebugPtr := flag.Bool("debug", false, "Enable to get more information")
	ViewPtr := flag.String("viewfolder", "assets/views", "Default directory for web views")
	IrisPtr := flag.String("iriscfg", "assets/iris.yml", "Path to IRIS configuration file")
	UrlBase := flag.String("urlbase", "helge.net", "Used for direct links to ip4 and ip6.urlbase")
	flag.Parse()
	// move configuration variables to configuration
	cfg.WebPort = *portPtr
	cfg.WebFolder = *WebPtr
	cfg.WebView = *ViewPtr
	cfg.Debug = *DebugPtr
	cfg.UrlBase = *UrlBase
	cfg.IrisCfg = *IrisPtr
	return &cfg
}

func LoadIrisConfiguration() iris.Configurator {
	// return default configuration if we do not load anything from command line
	if cfg.IrisCfg == "" {
		return iris.WithConfiguration(iris.Configuration{
			DisableVersionChecker: true,
		})
	}
	// load configuration from command line
	return iris.WithConfiguration(iris.YAML(cfg.IrisCfg))
}