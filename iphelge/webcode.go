package iphelge

import (
	"github.com/kataras/iris"
	"time"
	"fmt"
	"github.com/kataras/iris/context"
)

// Check regexp: https://regex101.com/

// used to cache the current year for the copyright notice
var currentyear int = time.Now().Year()

// A minimalistic logger that print calls to stdout, for debug purposes
func logger(ctx iris.Context) {
	fmt.Printf("%s %s %s\n", ctx.Method(), ctx.Host(), ctx.Path())
	ctx.Next()
}

// the front page display
func frontPage(ctx iris.Context) {
	ctx.ViewData("IP", ctx.RemoteAddr())
	ctx.ViewData("currentYear", currentyear)
	ctx.ViewData("baseUrl", cfg.UrlBase)
	ctx.View("index.html")
}

// A 404 error handler
func notFoundHandler(ctx iris.Context) {
	ctx.View("404.html")
}

// The struct that is returned back to the client
type Info struct {
	IP string
}

// Handler for returning current IP address
func getIP(ctx iris.Context) {
	ct := ctx.Request().Header.Get(context.ContentTypeHeaderKey)
	theIP := ctx.RemoteAddr()
	i := Info{IP: theIP}
	if ct == "text/xml" {
		ctx.XML(i)
		return
	}
	ctx.JSON(i)

}
